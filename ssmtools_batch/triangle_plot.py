#!/usr/bin/env python

import sys
#import os
#os.environ['PATH'] += ':/Library/TeX/texbin'
#sys.path.append('/proj/gowling/GW_tools')
#import GW_tools as g 

import matplotlib.pyplot as plt
import getdist as gd
import numpy as np 
print(gd.__file__)
import getdist.plots as gdplt
import os
from cobaya.yaml import yaml_load_file
plt.rcParams.update({'font.size': 50})



def tobs( n_years): 
    yr=365.25*86400.

    return n_years*yr

def main():
    info = yaml_load_file(sys.argv[1])
    folder, name = os.path.split(os.path.abspath(info["output"]))
    vw = info["fiducial"]["vw"]
    alpha = info["fiducial"]["alpha"]
    rs = info["fiducial"]["rs"]
    fiducial_params =[vw,alpha,rs] 
    

    save_string = 'plots/vw_{}alpha{}rs{}'.format(vw,alpha,rs).replace(".","_") +'.png'
    if not os.path.exists(data_dir):
  

    log10_alpha =np.log10(alpha) 
    log10_rs = np.log10(rs)
    

    fiducial = {"vw":vw,"log10_alpha":log10_alpha,"log10_rs":log10_rs}
    gdplot = gdplt.getSubplotPlotter(rc_sizes= True,chain_dir=folder,analysis_settings={'ignore_rows': 0.3} )
    gdplot.triangle_plot(name, ["vw", "log10_alpha","log10_rs"], filled=True,markers=fiducial )

    plt.savefig(save_string)



if __name__ == '__main__':
    main()
