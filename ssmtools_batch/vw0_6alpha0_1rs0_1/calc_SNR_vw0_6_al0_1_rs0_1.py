#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 11:25:09 2020

@author: cg411
"""

import sys, platform, os
sys.path.append('/proj/gowling/GW_tools')


import numpy as np 

import GW_tools as g 

def tobs( n_years): 
    yr=365.25*86400.

    return n_years*yr


t_obs = tobs(3)
freqs = np.logspace(-5,-1,num=1000,base =10)


vw_fiducial = 0.6
log10_alpha_fiducial = -1.0  
log10_rs_fiducial = -1.0
print('vw=',vw_fiducial)
print('alpha =', 10**(log10_alpha_fiducial))
print('rs =', 10**(log10_rs_fiducial))
fiducial_model = g.calc_omgw0(freqs,vw_fiducial,10**log10_alpha_fiducial,10**log10_rs_fiducial)
SNR = g.calc_SNR(freqs, fiducial_model, g.LISA_noise_curve_om(freqs),t_obs )

print(SNR)
