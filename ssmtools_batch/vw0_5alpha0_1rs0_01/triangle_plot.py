#!/usr/bin/env python


import sys
#import numpy as np
#import matplotlib as mpl
import matplotlib.pyplot as plt
import getdist as gd
import numpy as np 
print(gd.__file__)
import getdist.plots as gdplt
import os
from cobaya.yaml import yaml_load_file
plt.rcParams.update({'font.size': 50})





def main():
  #  if not len(sys.argv) in [4]:
 #       sys.stderr.write('usage: %s <v_wall> <alpha_n> [save_string]\n' % sys.argv[0])
#        sys.exit(1)
    info = yaml_load_file(sys.argv[1])
    folder, name = os.path.split(os.path.abspath(info["output"]))
    vw = info["fiducial"]["vw"]#float(sys.argv[2])
    alpha = info["fiducial"]["alpha"]#float(sys.argv[3])
    rs = info["fiducial"]["rs"]#float(sys.argv[4])


    save_string = 'vw_{}alpha{}rs{}'.format(vw,alpha,rs).replace(".","_") +'.png'

    log10_alpha =np.log10(alpha) 
    log10_rs = np.log10(rs)

    fiducial = {"vw":vw,"log10_alpha":log10_alpha,"log10_rs":log10_rs}
    gdplot = gdplt.getSubplotPlotter(rc_sizes= True,chain_dir=folder,analysis_settings={'ignore_rows': 0.2} )
    gdplot.triangle_plot(name, ["vw", "log10_alpha","log10_rs"], filled=True,markers=fiducial )

    plt.savefig(save_string)


if __name__ == '__main__':
    main()
