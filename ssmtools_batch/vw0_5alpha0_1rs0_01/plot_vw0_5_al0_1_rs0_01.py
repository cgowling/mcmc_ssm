#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 17:20:17 2020

@author: cg411
"""
import matplotlib.pyplot as plt
import getdist as gd
import numpy as np
print(gd.__file__)
import getdist.plots as gdplt

from cobaya.yaml import yaml_load_file
plt.rcParams.update({'font.size': 50})


info = yaml_load_file("ssmtools_vw0_5_al0_1_rs0_01.yaml")

import os

folder, name = os.path.split(os.path.abspath(info["output"]))

gdplot = gdplt.getSubplotPlotter(rc_sizes= True,chain_dir=folder,analysis_settings={'ignore_rows': 0.2} )


vw=0.5
alpha =0.1
rs =0.01

log10_alpha =np.log10(alpha) 
log10_rs = np.log10(rs)
 
fiducial = {"vw":vw,"log10_alpha":log10_alpha,"log10_rs":log10_rs}
gdplot.triangle_plot(name, ["vw", "log10_alpha","log10_rs"], filled=True,markers=fiducial )

plt.savefig('vw0_5_al0_1_rs0_01.png')
