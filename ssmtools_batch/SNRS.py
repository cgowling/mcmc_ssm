#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 11:14:32 2020

@author: cg411
"""

import sys, platform, os

sys.path.append('../../../gw_data_tools')
import numpy as np
import gwtools as g 


def tobs( n_years): 
    yr=365.25*86400.

    return n_years*yr



vws = [0.5, 0.6,0.7,0.8,0.9]
alphas = [0.05,0.1,]
rss = [0.01,0.1]
Tc= 1000
          #%%
SNRs = np.zeros([len(vws),len(alphas), len(rss)])          


#%%t_obs = tobs(3)
freqs = np.logspace(-5,-1,num=1000,base =10)
t_obs = tobs(3)
for i, vw in enumerate(vws):
    for j,alpha in enumerate(alphas):
        for k,rs in enumerate(rss):
        
            fiducial_model = g.calc_omgw0_T_dep(freqs,vw,alpha,rs,Tc)
            SNRs[i,j,k] = g.calc_SNR(freqs, fiducial_model, g.LISA_noise_curve_om(freqs),t_obs )
                      
              
          
