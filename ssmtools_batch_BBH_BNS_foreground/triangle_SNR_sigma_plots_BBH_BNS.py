#!/usr/bin/env python

import sys
#os.environ['PATH'] += ':/Library/TeX/texbin'
sys.path.append('/proj/gowling/gw_data_tools')
#import GW_tools as g 

sys.path.append('../../')
sys.path.append('/Users/cg411/Documents/PhD/Phd/gwaves_phasetrans/CG/MCMC_SSM')
sys.path.append('/Users/cg411/Documents/PhD/Phd/gwaves_phasetrans/CG/gw_data_tools')
import gwtools as g 
from getdist.mcsamples import loadMCSamples
import matplotlib.pyplot as plt
import getdist as gd
import numpy as np 
print(gd.__file__)
import getdist.plots as gdplt
import os
from cobaya.yaml import yaml_load_file
plt.rcParams.update({'font.size': 15})




def filter_lower_bound(bound,info):
    gd_sample = loadMCSamples(os.path.abspath(info["output"]))
    p= gd_sample.getParams()
    filtered_vw = gd_sample.filter(p.vw<bound)
    p2= gd_sample.getParams()
    index_best_fit = np.argmax(p2.chi2)
    alpha = 10**(p2.log10_alpha[index_best_fit])
    rs = 10**(p2.log10_rs[index_best_fit])
    vw = p2.vw[index_best_fit]
    params = [vw,alpha,rs]
    print(params)
    return params

def filter_upper_bound(bound,info):
    gd_sample = loadMCSamples(os.path.abspath(info["output"]))
    p= gd_sample.getParams()
    filtered_vw = gd_sample.filter(p.vw>bound)
    p2= gd_sample.getParams()
    index_best_fit = np.argmin(p2.chi2)
    alpha = 10**(p2.log10_alpha[index_best_fit])
    rs = 10**(p2.log10_rs[index_best_fit])
    vw = p2.vw[index_best_fit]
    params = [vw,alpha,rs]
    print(params)
    return params
    
def plot_gw_SSM_paramlist(freqs, param_array ):
    """
    param-array given in the format 
    x =[[vw1,alpha1,rs1],[vw2,alpha2,rs1]]
    """
    g.get_plot_with_sensitivity(freqs,power_law=True, GB =False, leg=False)
    colours = ['yellowgreen', 'seagreen','dodgerblue','mediumvioletred','darkorchid' , 'mediumblue']
    sigmas = [r'$-2\sigma$', r'$-\sigma$','fiducial', r'$+\sigma$',r'$+2\sigma$']
    lines = [1,2,3,4,5,6]
    for n in range(len(param_array)):
        params = param_array[n]
        omgw = g.calc_omgw0(freqs,params[0],params[1],params[2])
        
        lines[n] =plt.loglog(freqs,omgw, color = colours[n], label= sigmas[n])
 
    plt.ylim(1e-15,1e-6)
    legend = plt.legend(handles=[lines[0][0],lines[1][0],lines[2][0],lines[3][0],lines[4][0]], loc='upper right')
    plt.tight_layout()
    
def tobs( n_years): 
    yr=365.25*86400.

    return n_years*yr

def main():
    freqs = np.logspace(-5,-1,num=1000,base =10)
    info = yaml_load_file(sys.argv[1])
    folder, name = os.path.split(os.path.abspath(info["output"]))
    vw = info["fiducial"]["vw"]
    alpha = info["fiducial"]["alpha"]
    rs = info["fiducial"]["rs"]
    fiducial_params =[vw,alpha,rs]
    
    log10_alpha =np.log10(alpha)
    log10_rs = np.log10(rs)
    
    
    SNR = g.calc_SNR(freqs, g.calc_omgw0(freqs,fiducial_params[0],fiducial_params[1],fiducial_params[2]),g.LISA_noise_curve_om(freqs),tobs(3))
    
    run_dir = './'

    os.chdir(run_dir)
    data_dir = './plots/vw_{}alpha{}rs{}_BBH_BNS'.format(vw,alpha,rs).replace(".","_") + os.sep
    #print(data_dir)
    save_string = data_dir +'vw_{}alpha{}rs{}_tri_plot_BBH_BNS'.format(vw,alpha,rs).replace(".","_") +'.png'
    
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
        print('Made new directory',data_dir)

    # -+ sigmas

    gd_sample = loadMCSamples(os.path.abspath(info["output"]))
    p= gd_sample.getParams()
    minus, plus =  gd_sample.twoTailLimits(p.vw, 0.68)
    two_minus, two_plus = gd_sample.twoTailLimits(p.vw, 0.95)




    params_plus = filter_upper_bound(plus,info)
    params_minus =filter_lower_bound(minus,info)
    params_2plus = filter_upper_bound(two_plus,info)
    params_2minus = filter_lower_bound(two_minus,info)

    param_array = [params_2minus,params_minus,fiducial_params,params_plus,params_2plus]
    print('param_array',param_array[:][0])
    print(np.shape(param_array))
    vw_alpha_rs = list(zip(param_array[0],param_array[1],param_array[2],param_array[3],param_array[4]))
    ax_vw_al = list(zip(vw_alpha_rs[0],vw_alpha_rs[1]))
    ax_vw_rs = list(zip(vw_alpha_rs[0],vw_alpha_rs[2]))
    ax_al_rs = list(zip(vw_alpha_rs[1],vw_alpha_rs[1]))
    print(ax_vw_rs)

    freqs = np.logspace(-5,-1,num=1000,base =10)
#param_list = [params_2minus,params_minus,fiducial_params,params_plus,params_2plus]
    
    
    plot_gw_SSM_paramlist(freqs,param_array)
    save_string_sigmas = data_dir + '/vw_{}alpha{}rs{}_sigma_plot'.format(vw,alpha,rs,vw,alpha,rs).replace(".","_") +'.png'
    plt.savefig(save_string_sigmas)


    fiducial = {"vw":vw,"log10_alpha":log10_alpha,"log10_rs":log10_rs}
    gdplot = gdplt.getSubplotPlotter(rc_sizes= True,chain_dir=folder,analysis_settings={'ignore_rows': 0.3} )
    gdplot.triangle_plot(name, ["vw", "log10_alpha","log10_rs","log10_A"], filled=True,markers=fiducial )

    snr_text = 'SNR = ' + '{0:.2f}'.format(SNR)

    
    ax =gdplot.subplots[1,1]
    ax.text(1,1.5,snr_text,transform=ax.transAxes)
    ax.text(1,1.3,r'$ T_* = 100 $ GeV',transform=ax.transAxes)

    ax_1_0 =gdplot.subplots[1,0]
    ax_1_0.scatter(vw_alpha_rs[0],np.log10(vw_alpha_rs[1]),c='k', marker='o')
    ax_2_0 =gdplot.subplots[2,0]
    ax_2_0.scatter(vw_alpha_rs[0],np.log10(vw_alpha_rs[2]),c='k', marker='o')
    ax_2_1 =gdplot.subplots[2,1]
    ax_2_1.scatter(np.log10(vw_alpha_rs[1]),np.log10(vw_alpha_rs[2]),c='k', marker='o')





        
    plt.savefig(save_string)




if __name__ == '__main__':
    main()
