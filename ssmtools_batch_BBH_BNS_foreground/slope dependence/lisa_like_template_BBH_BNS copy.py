#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 11:25:09 2020

@author: cg411
"""
import sys, platform, os

sys.path.append('/proj/gowling/gw_data_tools')
sys.path.append('/proj/gowling')

import numpy as np
import gwtools as g


# define fiducial model 
#vw_fiducial =
#log10_alpha_fiducial =
#log10_rs_fiducial = 
#log10_A_fiducial =
#slope_fiducial =


def chi_squared(freqs,om_n,t_obs, vw, alpha,rs,A,slope,fiducial_model):# fiducial_omega=0
    
    
    theory_omega = g.calc_omgw0(freqs,vw,alpha,rs) + g.om_compact_binary_ligo_variable(freqs,A,slope)
    x =(theory_omega-fiducial_model)**2/om_n**2
    int_res = np.trapz(x,freqs)
    chisquared = t_obs * int_res
    return chisquared


def lisa(vw, log10_alpha, log10_rs,log10_A,slope ):
    return -chi_squared(freqs,Om_n,t_obs,vw,10**log10_alpha,10**log10_rs,10**log10_A_fiducial,slope_fiducial, fiducial_model)/2


def tobs( n_years): 
    yr=365.25*86400.

    return n_years*yr

#%%

t_obs = tobs(3)
freqs = np.logspace(-5,-1,num=1000,base =10)


Om_n  =g.LISA_noise_curve_om(freqs) + g.om_compact_binary_ligo_variable(freqs,10**log10_A_fiducial,slope_fiducial)


fiducial_model = g.calc_omgw0(freqs,vw_fiducial,10**log10_alpha_fiducial,10**log10_rs_fiducial) +g.om_compact_binary_ligo_variable(freqs,10**log10_A_fiducial,slope_fiducial)




