#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 11:25:09 2020

@author: cg411
"""
import sys, platform, os

sys.path.append('/proj/gowling/gw_data_tools')
sys.path.append('/proj/gowling')

import numpy as np
import gwtools as g


# define fiducial model 
vw_fiducial = 0.8
log10_alpha_fiducial = -1.3010299956639813
log10_rs_fiducial = -2.0 
log10_A_fiducial = -9.676687500678053



def chi_squared(freqs,om_n,theory_omega,fiducial_model,t_obs):# fiducial_omega=0
    
    x =(theory_omega-fiducial_model)**2/om_n**2
    int_res = np.trapz(x,freqs)
    chisquared = t_obs * int_res
    return chisquared


def lisa(vw, log10_alpha, log10_rs,log10_A):
    # theory , get fid,om
    theory_omega = g.calc_omgw0(freqs,vw,10**log10_alpha,10**log10_rs) + g.om_compact_binary_ligo(freqs,10**log10_A)
    Om_n  =Om_lisa_sens_curve + g.om_compact_binary_ligo(freqs,10**log10_A)
    return -chi_squared(freqs,Om_n,theory_omega,fiducial_model,t_obs)/2


def tobs( n_years): 
    yr=365.25*86400.

    return n_years*yr

#%%

t_obs = tobs(3)
freqs = np.logspace(-5,-1,num=1000,base =10)

Om_lisa_sens_curve =g.LISA_noise_curve_om(freqs)

fiducial_model = g.calc_omgw0(freqs,vw_fiducial,10**log10_alpha_fiducial,10**log10_rs_fiducial) +g.om_compact_binary_ligo(freqs,10**log10_A_fiducial)




