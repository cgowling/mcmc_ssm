#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 15:05:01 2020

@author: hindmars
"""

# Creates a set of folders and run files from a list of parameters
#  parameter file assumed to be 'params.txt'
#  template file for python likelihood  assumed to be 'lisa_like_template.py'
# template for .yaml assumed to be ssm_thermo.yaml
# lots of improvements possible

import os
import numpy as np
import yaml
import random as r


def make_dict_for_yaml(p):
    # There must be a way to do this directly from a structured array
    # note that p[:] is type numpy.float, and yaml.dump will not cope
    with open("ssmtools_template_T_dep.yaml") as f:
     info = yaml.load(f, Loader=yaml.FullLoader)
    vw_ref = float(p[0]*(1+(r.random()-0.5)*0.01))
    log_10_alpha_ref = float(np.log10(p[1])*(1+(r.random()-0.5)*0.01))
    log_10_rs_ref = float(np.log10(p[2])*(1+(r.random()-0.5)*0.01))
    log_10_T_ref = float(np.log10(p[3])*(1+(r.random()-0.5)*0.01))
    info["params"]["vw"]["ref"] = vw_ref 
    info["params"]["log10_alpha"]["ref"] = log_10_alpha_ref 
    info["params"]["log10_rs"]["ref"] = log_10_rs_ref
    info["params"]["log10_T"]["ref"] = log_10_T_ref
    info["output"] = data_dir +'chains/chain'
    if not os.path.exists(data_dir + 'chains/'):
        os.makedirs(data_dir + 'chains')
        print('Made new directory ' + data_dir + 'chains')
    path_from_ssmtools_batch = 'vw{}alpha{}rs{}_T{}/'
    module_path =path_from_ssmtools_batch.format(p[0],p[1],p[2],p[3]).replace(".","_").replace("/",".") + like_name_template.format(p[0],p[1],p[2],p[3]).replace(".","_")
    info["likelihood"]["lisa_likelihood"] = "import_module('"+module_path + "').lisa"
    info["fiducial"]["vw"] = float( p[0])
    info["fiducial"]["alpha"] = float(p[1])
    info["fiducial"]["rs"] = float(p[2])
    info["fiducial"]["T"] = float(p[3])
    
    return info


def make_lisa_like_file_text(p):

    f = open('lisa_like_template_T_dep.py','r')
    wrap_text = f.read()

    wrap_text = wrap_text.replace("#vw_fiducial =", "vw_fiducial = " + str(p[0]))
    wrap_text = wrap_text.replace("#log10_alpha_fiducial =", "log10_alpha_fiducial = " + str(np.log10(p[1])))
    wrap_text = wrap_text.replace("#log10_rs_fiducial =", "log10_rs_fiducial = " + str(np.log10(p[2])))
    wrap_text = wrap_text.replace("#log10_T_fiducial =", "log10_T_fiducial = " + str(np.log10(p[3])))

    f.close()

    return wrap_text

param_file = 'params_T_dep.txt'
run_dir = '/proj/gowling/MCMC_SSM/ssmtools_batch_Tdep'
#/proj/gowling/ssmtools_batch'

os.chdir(run_dir)


param_struct = np.genfromtxt(param_file, names=True)
 

# This is a structured array.

data_name_template = 'vw{}alpha{}rs{}_T{}'
yaml_template = 'ssmtools_vw{}_al{}_rs{}_T{}'
like_name_template = 'lisa_like_vw{}_al{}_rs{}_T{}'


for n, p in enumerate(param_struct):

    data_dir = data_name_template.format(p[0],p[1],p[2],p[3]).replace(".","_") + os.sep
    yaml_file = yaml_template.format(p[0],p[1],p[2],p[3]).replace(".","_") + '.yaml'
    like_file = like_name_template.format(p[0],p[1],p[2],p[3]).replace(".","_") + '.py'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
        print('Made new directory ' + data_dir)
    if not os.path.exists(data_dir+'results'):
        os.makedirs(data_dir + 'results')
        print('Made new directory ' + data_dir +'results')    
    yaml_path = data_dir + yaml_file
    like_path = data_dir + like_file
    
    info = make_dict_for_yaml(p)
    with open(yaml_path, 'w') as f:
        yout = yaml.dump(info, f)
        print('Made yaml file       ' + yaml_path)
    
        
        
    like_file_text = make_lisa_like_file_text(p)
    with open(like_path,'w') as fo:
        fo.writelines(like_file_text)
        fo.close
        print('Made likelihood file ' + like_path)
# all done
    
